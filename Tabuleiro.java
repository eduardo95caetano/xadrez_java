public class Tabuleiro {
    
    public int linhas;
    public int colunas;
    public Jogador jogador;

    public Tabuleiro(int linhas, int colunas) {
        this.linhas = linhas;
        this.colunas = colunas;
    }

    public void mostrarTabuleiro(Jogador Jogador) {
        for (int i = 0; i < linhas; i++) {

            for (int j = 0; j < colunas; j++) {

                if (jogador.posiX == 1 && jogador.posiY == 2) {

                    System.out.println("x");
                    
                }else {

                    System.out.printf("[.]");

                }
                
            }
            System.out.println();
        }
    }
    

    public int getLinhas() {
        return linhas;
    }

    public void setLinhas(int linhas) {
        this.linhas = linhas;
    }

    public int getColunas() {
        return colunas;
    }

    public void setColunas(int colunas) {
        this.colunas = colunas;
    }
}

