public class Jogador {
    
    public int posiX = 1;
    public int posiY = 2;

    
    public Jogador(int posiX, int posiY) {
        this.posiX = posiX;
        this.posiY = posiY;
    }


    public int getPosiX() {
        return posiX;
    }


    public void setPosiX(int posiX) {
        this.posiX = posiX;
    }


    public int getPosiY() {
        return posiY;
    }


    public void setPosiY(int posiY) {
        this.posiY = posiY;
    }

    
}
